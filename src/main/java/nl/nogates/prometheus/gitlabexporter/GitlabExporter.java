package nl.nogates.prometheus.gitlabexporter;

import io.prometheus.client.exporter.HTTPServer;
import nl.nogates.prometheus.gitlabexporter.collector.LatestPipelineStateCollector;
import nl.nogates.prometheus.gitlabexporter.collector.ProjectStatisticsCollector;
import org.gitlab.api.GitlabAPI;

import java.io.IOException;

import static java.lang.System.getenv;

public class GitlabExporter {
    private static final String GITLAB_HOST_URL   = getenv("GITLAB_EXPORTER_HOST_URL");
    private static final String GITLAB_API_TOKEN  = getenv("GITLAB_EXPORTER_API_TOKEN");

    public static void main(String[] args) throws IOException {
        new HTTPServer(9345);

        var gitlab = GitlabAPI.connect(GITLAB_HOST_URL, GITLAB_API_TOKEN);

        LatestPipelineStateCollector.register(gitlab);
        ProjectStatisticsCollector.register(gitlab);
    }
}
