package nl.nogates.prometheus.gitlabexporter.collector;

import io.prometheus.client.Collector;
import io.prometheus.client.GaugeMetricFamily;
import nl.nogates.gitlab.model.GitlabPipeline;
import org.gitlab.api.GitlabAPI;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.joining;
import static nl.nogates.gitlab.model.GitlabPipeline.pipelines;
import static nl.nogates.gitlab.model.GitlabProject.projects;

public class LatestPipelineStateCollector extends Collector {
    private final GitlabAPI gitlab;

    private LatestPipelineStateCollector(GitlabAPI gitlab) {
        this.gitlab = gitlab;
    }

    public static void register(GitlabAPI gitlab) {
        new LatestPipelineStateCollector(gitlab).register();
    }

    private String describePipelineStatus() {
        return Arrays.stream(GitlabPipeline.PipelineStatus.values())
                     .map(s -> s.ordinal() + "=" + s.name())
                     .collect(joining(", "));
    }

    @Override
    public List<MetricFamilySamples> collect() {
        var metricFamily = new GaugeMetricFamily("gitlab_latest_pipeline_state",
                "The state of the latest pipeline of each ref: " + describePipelineStatus(),
                asList("group", "project", "ref"));

        projects(gitlab).forEach(proj ->
                pipelines(gitlab, proj, GitlabPipeline.PipelineScope.branches).forEach(p ->
                        metricFamily.addMetric(
                                asList(proj.getNamespace().getPath(), proj.getPathWithNamespace(), p.getRef()),
                                p.getStatus().ordinal()))
        );

        return Collections.singletonList(metricFamily);
    }
}
