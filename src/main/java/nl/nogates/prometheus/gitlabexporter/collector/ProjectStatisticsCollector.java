package nl.nogates.prometheus.gitlabexporter.collector;

import io.prometheus.client.Collector;
import io.prometheus.client.CounterMetricFamily;
import io.prometheus.client.GaugeMetricFamily;
import nl.nogates.gitlab.model.GitlabProject;
import org.gitlab.api.GitlabAPI;

import java.util.List;

import static java.util.Arrays.asList;

public class ProjectStatisticsCollector extends Collector {
    private final GitlabAPI gitlab;

    private ProjectStatisticsCollector(GitlabAPI gitlab) {
        this.gitlab = gitlab;
    }

    public static void register(GitlabAPI gitlab) {
        new ProjectStatisticsCollector(gitlab).register();
    }

    @Override
    public List<MetricFamilySamples> collect() {
        List<String> labelNames = asList("group", "project");
        var commitCount = new CounterMetricFamily("gitlab_project_statistics_commit_count",
                "Gitlab Project number of commits", labelNames);
        var artifactsSize = new GaugeMetricFamily("gitlab_project_statistics_job_artifact_bytes",
                "Gitlab Project job artifact size in bytes", labelNames);
        var lfsObjectsSize = new GaugeMetricFamily("gitlab_project_statistics_lfs_objects_bytes",
                "Gitlab Project LFS object size in bytes", labelNames);
        var repositorySize = new GaugeMetricFamily("gitlab_project_statistics_repository_bytes",
                "Gitlab Project repository size in bytes", labelNames);
        var storageSize = new GaugeMetricFamily("gitlab_project_statistics_storage_bytes",
                "Gitlab Project storage size in bytes", labelNames);
        var lastActivity = new CounterMetricFamily("gitlab_project_statistics_last_activity_at_seconds",
                "Gitlab Project last activity timestamp", labelNames);
        var starCount = new GaugeMetricFamily("gitlab_project_statistics_star_count",
                "Gitlab Project Star count", labelNames);
        var forksCount = new GaugeMetricFamily("gitlab_project_statistics_forks_count",
                "Gitlab Project Forks count", labelNames);

        GitlabProject.projects(gitlab)
                     .forEach(proj -> {
                         List<String> labelValues = asList(proj.getNamespace().getPath(), proj.getPathWithNamespace());
                         if (proj.getStatistics() != null) {
                             commitCount.addMetric(labelValues,
                                     proj.getStatistics().getCommitCount().doubleValue());
                             artifactsSize.addMetric(labelValues,
                                     proj.getStatistics().getJobArtifactsSize().doubleValue());
                             if (proj.getStatistics().getLfsObjectsSize() != null) {
                                 lfsObjectsSize.addMetric(labelValues,
                                         proj.getStatistics().getLfsObjectsSize().doubleValue());
                             }
                             repositorySize.addMetric(labelValues,
                                     proj.getStatistics().getRepositorySize().doubleValue());
                             storageSize.addMetric(labelValues,
                                     proj.getStatistics().getStorageSize().doubleValue());
                         }
                         lastActivity.addMetric(labelValues,
                                 proj.getLastActivityAt().getTime() / 1000.0);
                         starCount.addMetric(labelValues,
                                 proj.getStarCount().doubleValue());
                         forksCount.addMetric(labelValues,
                                 proj.getForksCount().doubleValue());
                     });

        return asList(commitCount, artifactsSize, lfsObjectsSize, repositorySize, storageSize, lastActivity, starCount, forksCount);
    }
}
