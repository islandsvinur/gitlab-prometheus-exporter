package nl.nogates.gitlab.model;

public interface GitlabIdentifiable {
    Integer getId();

    void setId(Integer id);
}
