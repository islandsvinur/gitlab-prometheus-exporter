package nl.nogates.gitlab.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GitlabProjectStatistics {

    @JsonProperty("commit_count")
    private Number commitCount;
    @JsonProperty("storage_size")
    private Number storageSize;
    @JsonProperty("repository_size")
    private Number repositorySize;
    @JsonProperty("lfs_object_size")
    private Number lfsObjectsSize;
    @JsonProperty("job_artifacts_size")
    private Number jobArtifactsSize;

    public Number getCommitCount() {
        return commitCount;
    }

    public void setCommitCount(Number commitCount) {
        this.commitCount = commitCount;
    }

    public Number getStorageSize() {
        return storageSize;
    }

    public void setStorageSize(Number storageSize) {
        this.storageSize = storageSize;
    }

    public Number getRepositorySize() {
        return repositorySize;
    }

    public void setRepositorySize(Number repositorySize) {
        this.repositorySize = repositorySize;
    }

    public Number getLfsObjectsSize() {
        return lfsObjectsSize;
    }

    public void setLfsObjectsSize(Number lfsObjectsSize) {
        this.lfsObjectsSize = lfsObjectsSize;
    }

    public Number getJobArtifactsSize() {
        return jobArtifactsSize;
    }

    public void setJobArtifactsSize(Number jobArtifactsSize) {
        this.jobArtifactsSize = jobArtifactsSize;
    }
}
