package nl.nogates.gitlab.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import nl.nogates.gitlab.GitlabStreamSupport;
import org.gitlab.api.GitlabAPI;
import org.gitlab.api.models.GitlabUser;

import java.util.Date;
import java.util.stream.Stream;

public class GitlabPipeline implements GitlabIdentifiable {

    @JsonProperty("id")
    private Integer id;
    private PipelineStatus status;
    private String ref;
    private String sha;
    @JsonProperty("before_sha")
    private String beforeSha;
    private Boolean tag;
    @JsonProperty("yaml_errors")
    private String yamlErrors;
    private GitlabUser user;
    @JsonProperty("created_at")
    private Date createdAt;
    @JsonProperty("updated_at")
    private Date updatedAt;
    @JsonProperty("started_at")
    private Date startedAt;
    @JsonProperty("finished_at")
    private Date finishedAt;
    @JsonProperty("committed_at")
    private Date committedAt;
    private Number duration;
    private Number coverage;
    @JsonProperty("web_url")
    private String webUrl;

    public static Stream<GitlabPipeline> pipelines(GitlabAPI gitlab, GitlabProject project, PipelineScope scope) {
        return GitlabStreamSupport.stream(gitlab, GitlabPipeline[].class,
                "/projects/" + project.getId() + "/pipelines?scope=" + scope.name());
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public PipelineStatus getStatus() {
        return status;
    }

    public void setStatus(PipelineStatus status) {
        this.status = status;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getSha() {
        return sha;
    }

    public void setSha(String sha) {
        this.sha = sha;
    }

    public String getBeforeSha() {
        return beforeSha;
    }

    public void setBeforeSha(String beforeSha) {
        this.beforeSha = beforeSha;
    }

    public Boolean getTag() {
        return tag;
    }

    public void setTag(Boolean tag) {
        this.tag = tag;
    }

    public String getYamlErrors() {
        return yamlErrors;
    }

    public void setYamlErrors(String yamlErrors) {
        this.yamlErrors = yamlErrors;
    }

    public GitlabUser getUser() {
        return user;
    }

    public void setUser(GitlabUser user) {
        this.user = user;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(Date startedAt) {
        this.startedAt = startedAt;
    }

    public Date getFinishedAt() {
        return finishedAt;
    }

    public void setFinishedAt(Date finishedAt) {
        this.finishedAt = finishedAt;
    }

    public Date getCommittedAt() {
        return committedAt;
    }

    public void setCommittedAt(Date committedAt) {
        this.committedAt = committedAt;
    }

    public Number getDuration() {
        return duration;
    }

    public void setDuration(Number duration) {
        this.duration = duration;
    }

    public Number getCoverage() {
        return coverage;
    }

    public void setCoverage(Number coverage) {
        this.coverage = coverage;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public enum PipelineScope {
        branches
    }

    public enum PipelineStatus {
        running, pending, success, failed, canceled, skipped, manual
    }
}
