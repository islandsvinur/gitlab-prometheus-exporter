package nl.nogates.gitlab;

import org.gitlab.api.GitlabAPI;

import java.util.Arrays;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static java.util.Spliterators.spliteratorUnknownSize;

public class GitlabStreamSupport {

    public static <T> Stream<T> stream(GitlabAPI gitlab, Class<T[]> modelArrayClass, String url) {
        return StreamSupport.stream(
                spliteratorUnknownSize(gitlab.retrieve().asIterator(url, modelArrayClass), 0),
                false)
                            .flatMap(Arrays::stream);
    }
}
